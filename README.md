# Base de données et Oracle

Lors des années précédentes, lors de la MSPR de base de données, il était attendu de travailler dans un environnement oracle. Ce qui a été compliqué pour la plupart des groupes.  Afin d'anticiper ce genre d'aléas pour vos futurs projets, je vous propose l'exercice suivant :

Installez oracle via docker. 
Vous pouvez utiliser le fichier `docker-compose.yml` ci-joint.
Migrer une base de données mysql et la livrer sur une base de données Oracle.
Ajouter un trigger au sein de la base de données oracle.

## Pré requis
Avoir réalisé l'exercice de base de données [i-garden](https://gitlab.com/docusland-courses/database/bdd-i-garden)

## Attendus de l'exercice
- Créer un utilisateur `igarden` avec le mot de passe `ilovetomatoes`.
- Migrez le script précédent au sein de cette base de données. 
- La procédure stockée devra également être migrée.
- Directement au sein d'Oracle, ajoutez un trigger permettant de suivre les changements opérés au sein de la table `Plant_has_Task`